
# 📺 Mubify 📺
  
Welcome to Mubify, your ultimate destination for all things movies! Dive into a comprehensive movie database where you can explore a vast collection of films from every genre and era. Mubify goes beyond a typical movie database. Create personalized movie playlists tailored to your mood, occasion, or genre preferences. 

![alt text](public/img/sc3.png)
![alt text](public/img/sc1.png)
![alt text](public/img/sc2.png)

## Prerequisites/Specifications
1. **[PHP ^8.2](https://windows.php.net/download//)**
2. **[Composer 2](https://getcomposer.org/download/)**
3. **[Laragon](https://laragon.org/download.html)** (pre-installed with PHP, MySQL, Apache/Nginx)
4. **[VS Code](https://code.visualstudio.com/download)** (or other code editors)
5. **[Laravel 10](https://laravel.com/docs/10.x)**

## Installation
1. Open terminal in root directory (www folder)
2. `git clone https://gitlab.com/aisyahhanifiah/mubify.git`
3. `cd mubify/`
4. `composer install`
5. Load and run `mubify.sql` (located at root folder) to your database.
6. `cp .env.example .env` verify your database credentials in .env 
7. Serve the application
   1. Start Laragon and navigate to mubify.test, or
   2. `php artisan serve` navigate to the given address you will see your application is running.
   
## Usage
Register a user or login with default user admin@mubify.com and password secret from your database and start browsing.


## Checklist
1. Async function using promise method `resources\views\movie\index.blade.php`
```
function fetchData() {
  return new Promise((resolve, reject) => {
    url = "{{ url('/load-more-movies/') }}";
    fetch(url + "?start=" + start + "&" + location.search.substr(1)) // Example API endpoint
      .then(response => {
        if (!response.ok) {
          throw new Error('Something is wrong.');
        }
        return response.json();
      })
      .then(data => {
        resolve(data); // Resolve with the fetched data
      })
      .catch(error => {
        reject(error); // Reject with the encountered error
      });
  });
}
```

2. Advance DB query demonstration `app\Http\Controllers\HomeController.php`
```
$top_five_playlists = DB::table('playlist_movies')
->select('playlist_movies.playlist_id', 'playlists.title', 'playlists.description', 'users.name', 'playlist_movies.movie_id', 'movies.backdrop_path', 'movies.popularity')
->leftJoin('playlists', 'playlists.id', '=', 'playlist_movies.playlist_id')
->leftJoin('movies', 'movies.id', '=', 'playlist_movies.movie_id')
->leftJoin('users', 'users.id', '=', 'playlists.creator_id')
->orderByRaw('ROW_NUMBER() OVER (PARTITION BY playlist_movies.playlist_id ORDER BY movies.popularity DESC)')
->limit(5)
->get();
// SELECT playlist_movies.playlist_id, playlists.title, playlists.description, users.name, playlist_movies.movie_id, movies.backdrop_path, movies.popularity
// FROM playlist_movies
// LEFT JOIN movies ON movies.id = playlist_movies.movie_id
// LEFT JOIN playlists ON playlists.id = playlist_movies.playlist_id
// LEFT JOIN users ON users.id = playlists.creator_id
// ORDER BY ROW_NUMBER() OVER (PARTITION BY playlist_movies.playlist_id ORDER BY movies.popularity DESC)
// LIMIT 5
// ;
```

3. AJAX content loading in modal `resources\views\movie\movie-modal.blade.php`
```
$('body').on('click', '#movieModalButton', function(event) {
    event.preventDefault();
    var id = $(this).data('id');
    $.get('/get-movie/' + id,
        function(data) {
            $('#movieModalLabel').html(data.original_title);
            $('#modal_overview').html(data.overview);
            $('#modal_genres').html(data.genre);
            $('#modal_release_date').html(moment(data.release_date).format('D MMMM YYYY'));
            $('#modal_vote_average').html(data.vote_average);
            $('#modal_runtime').html(data.runtime);
            $('#modal_budget').html(formatter.format(data.budget));
            $('#modal_revenue').html(formatter.format(data.revenue));
            $("#modal_poster_path").prop("src", data.poster_path);
        })
});
```

4. Carousel content card `resources\views\pages\dashboard.blade.php`
5. Populate ChartJS with MySQL data using AJAX `resources\views\pages\dashboard.blade.php`
6. Export Excel/PDF `resources\views\user\index.blade.php`
7. Welcome user notification by bell `app\Notifications\WelcomeUserNotification.php`

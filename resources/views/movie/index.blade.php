

@extends('layouts.app')

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => 'Movies'])
    <div class="row mt-4 mx-4">
        <div class="col-12">
            @if (session()->has('status'))
                <div class="alert alert-light" role="alert">
                    {{ session('message') }}
                </div>
            @endif

            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6 class="mb-0">Search</h6>
                </div>
                <div class="card-body pt-0 pb-2">
                    <form action="{{ route('movie.index') }}" method="GET">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Title</label>
                                    <input class="form-control" type="text" id="title" name="title" value="{{ old('title', $request->title) }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Genre</label>
                                    <select class="form-control" id="genre" class="genre" name="genre" style="width: 100%" >
                                        <option value="">Please Choose</option>

                                        @foreach ($genres as $genre)
                                            <option value="{{ $genre->id }}" @selected($genre->id == $request->genre)>{{ $genre->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm ms-auto">Search</button>
                    </form>
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>Movies</h6>
                    {{-- <div class="d-flex align-items-center">
                            <a href="{{ route('movie.create') }}" class="btn btn-primary btn-sm mx-1">Add New Movie</a>

                    </div> --}}
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="row" id="items_container">
                        @foreach ($movies as $movie)
                        <div class="col-md-3 mt-4">
                            <div class="card">
                              <img class="card-img-top" src="{{ $movie->poster_path }}">
                              <div class="card-body">
                                <h4>
                                    {{ $movie->original_title }}
                                </h4>
                                <p>
                                    {{ $movie->release_date->format('j F Y') }}
                                </p>
                                <a href="" class="pe-2" data-bs-toggle="modal"
                                    data-bs-target="#movieModal" id="movieModalButton"
                                    data-id="{{ $movie->id }}">
                                    <i class="fa fa-solid fa-eye"></i> More info
                                </a>
                              </div>
                            </div>
                          </div>
                        @endforeach
                    </div>

                    @if($movies->count() == 8)
                    <div class="col-md-12 mt-4 text-center">
                            <button id="load_more_button" data-page="{{ $movies->currentPage() + 1 }}"
                                class="btn btn-primary btn-lg btn-block">Load More</button>
                    </div>
                    @endif

                </div>
                @include('movie.movie-modal')
            </div>
        </div>
    </div>
@endsection

@push('after-scripts')
<script>
    $(document).ready(function() {
        $('#genre').select2();

        function fetchData() {
          return new Promise((resolve, reject) => {
            url = "{{ url('/load-more-movies/') }}";
            fetch(url + "?start=" + start + "&" + location.search.substr(1)) // Example API endpoint
              .then(response => {
                if (!response.ok) {
                  throw new Error('Something is wrong.');
                }
                return response.json();
              })
              .then(data => {
                resolve(data); // Resolve with the fetched data
              })
              .catch(error => {
                reject(error); // Reject with the encountered error
              });
          });
        }

        var start = 8;

        $('#load_more_button').click(function() {
            // Using the fetchData function
            fetchData()
              .then(data => {
                // Handle the fetched data asynchronously
                if (data.data.length > 0) {
                    var html = '';
                    for (var i = 0; i < data.data.length; i++) {
                        html += `<div class="col-md-3 mt-4">
                            <div class="card">
                              <img class="card-img-top" src="` + data.data[i].poster_path + `">
                              <div class="card-body">
                                <h4>
                                    ` + data.data[i].original_title + `
                                </h4>
                                <p>
                                    ` + moment(data.data[i].release_date).format('D MMMM YYYY') + `
                                </p>
                                <a href="" class="pe-2" data-bs-toggle="modal"
                                    data-bs-target="#movieModal" id="movieModalButton"
                                    data-id="` + data.data[i].id + `">
                                    <i class="fa fa-solid fa-eye"></i> More info
                                </a>
                              </div>
                            </div>
                          </div>`;
                    }

                    //append data with fade in effect
                    $('#items_container').append($(html).hide().fadeIn(1000));
                    $('#load_more_button').html('Load More');
                    $('#load_more_button').attr('disabled', false);
                    start = data.next;
                    console.log(data.next);
                } else {
                    $('#load_more_button').html('No More Data Available');
                    $('#load_more_button').attr('disabled', true);
                }
                // Perform actions with the fetched data or update the DOM asynchronously
              })
              .catch(error => {
                // Handle errors if the promise is rejected
                console.error('Error fetching data:', error);
                // Perform error handling or display an error message asynchronously
              });

        });




        // var start = 8;

        // $('#load_more_button').click(function() {
        //     $.ajax({
        //         url: "{{ route('load.more.movies') }}",
        //         method: "GET",
        //         data: {
        //             start: start
        //         },
        //         dataType: "json",
        //         beforeSend: function() {
        //             $('#load_more_button').html('Loading...');
        //             $('#load_more_button').attr('disabled', true);
        //         },
        //         success: function(data) {
        //             if (data.data.length > 0) {
        //                 var html = '';
        //                 for (var i = 0; i < data.data.length; i++) {
        //                     html += `<div class="col-md-3 mt-4">
        //                         <div class="card">
        //                           <img class="card-img-top" src="` + data.data[i].poster_path + `">
        //                           <div class="card-body">
        //                             <h4>
        //                                 ` + data.data[i].original_title + `
        //                             </h4>
        //                             <p>
        //                                 ` + moment(data.data[i].release_date).format('D MMMM YYYY') + `
        //                             </p>
        //                             <a href="javascript:;" class="text-primary icon-move-right">More about us
        //                               <i class="fas fa-arrow-right text-xs ms-1" aria-hidden="true"></i>
        //                             </a>
        //                           </div>
        //                         </div>
        //                       </div>`;
        //                 }

        //                 //append data with fade in effect
        //                 $('#items_container').append($(html).hide().fadeIn(1000));
        //                 $('#load_more_button').html('Load More');
        //                 $('#load_more_button').attr('disabled', false);
        //                 start = data.next;
        //                  console.log(data.next);
        //             } else {
        //                 $('#load_more_button').html('No More Data Available');
        //                 $('#load_more_button').attr('disabled', true);
        //             }
        //         }
        //     });
        // });


        $('#reset-btn').click(function(event) {
            $("#username").val('');
            $("#kp").val('');
            $("#nama").val('');
        });

    });

</script>
@endpush

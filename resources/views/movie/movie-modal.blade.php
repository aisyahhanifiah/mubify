<div class="modal fade" id="movieModal" tabindex="-1" role="dialog" aria-labelledby="movieModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="movieModalLabel"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <img id="modal_poster_path" style="max-width: 50%" src="" alt="">
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="modal_overview">Overview</label>
                            <p class="text-primary text-small ms-1" id="modal_overview"></p>
                        </div>
                        <div class="form-group">
                            <label for="modal_genres">Genre</label>
                            <p class="text-primary text-small ms-1" id="modal_genres"></p>
                        </div>
                        <div class="form-group">
                            <label for="modal_vote_average">Vote Average</label>
                            <p class="text-primary text-small ms-1" id="modal_vote_average"></p>
                        </div>
                        <div class="form-group">
                            <label for="modal_release_date">Release Date</label>
                            <p class="text-primary text-small ms-1" id="modal_release_date"></p>
                        </div>
                        <div class="form-group">
                            <label for="modal_budget">Budget</label>
                            <p class="text-primary text-small ms-1" id="modal_budget"></p>
                        </div>
                        <div class="form-group">
                            <label for="modal_revenue">Revenue</label>
                            <p class="text-primary text-small ms-1" id="modal_revenue"></p>
                        </div>
                        <div class="form-group">
                            <label for="modal_runtime">Runtime</label>
                            <p class="text-primary text-small ms-1" id="modal_runtime"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@push('after-scripts')
    <script>
        $(document).ready(function() {
            const formatter = new Intl.NumberFormat('en-US', {
              style: 'currency',
              currency: 'USD',
            });

            $('body').on('click', '#movieModalButton', function(event) {
                event.preventDefault();
                var id = $(this).data('id');
                $.get('/get-movie/' + id,
                    function(data) {
                        $('#movieModalLabel').html(data.original_title);
                        $('#modal_overview').html(data.overview);
                        $('#modal_genres').html(data.genre);
                        $('#modal_release_date').html(moment(data.release_date).format('D MMMM YYYY'));
                        $('#modal_vote_average').html(data.vote_average);
                        $('#modal_runtime').html(data.runtime);
                        $('#modal_budget').html(formatter.format(data.budget));
                        $('#modal_revenue').html(formatter.format(data.revenue));
                        $("#modal_poster_path").prop("src", data.poster_path);
                    })
            });
        });
    </script>
@endpush

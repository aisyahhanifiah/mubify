<footer class="footer pt-3  ">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-lg-between">
            <div class="col-lg-6 mb-lg-0 mb-4">
                <div class="copyright text-center text-sm text-muted text-lg-start">
                    ©
                    <script>
                        document.write(new Date().getFullYear())
                    </script>,
                    made with <i class="fa fa-heart"></i> for
                    <a href="https://www.3ds.com/" class="font-weight-bold" target="_blank">Dassault Systèmes</a>
                </div>
            </div>
            <div class="col-lg-6">
                <ul class="nav nav-footer justify-content-center justify-content-lg-end">
                    <li class="nav-item">
                        <a href="https://gitlab.com/aisyahhanifiah/mubify" class="nav-link text-muted" target="_blank">Mubify on Gitlab</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

{{-- <html>
    <head>

    </head>
    <body>
    <h1>Senarai Pengguna</h1>
    <a href="{{ route('user.create') }}"><button>Create</button></a>
        <table>
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        <a href="{{ route('user.show', $user->id) }}"><button>Show</button></a>

                        <br>

                        <a href="{{ route('user.edit', $user->id) }}"><button>Edit</button></a>

                        <form method="POST" action="{{ route('user.destroy', $user->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <button type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html> --}}


@extends('layouts.app')

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => 'User Management'])
    <div class="row mt-4 mx-4">
        <div class="col-12">
            {{-- <div class="alert alert-light" role="alert">
                This feature is available in <strong>Argon Dashboard 2 Pro Laravel</strong>. Check it
                <strong>
                    <a href="https://www.creative-tim.com/product/argon-dashboard-pro-laravel" target="_blank">
                        here
                    </a>
                </strong>
            </div> --}}
            @if (session()->has('status'))
                <div class="alert alert-light" role="alert">
                    {{ session('message') }}
                </div>
            @endif

            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6 class="mb-0">Search</h6>
                </div>
                <div class="card-body pt-0 pb-2">
                    <form action="{{ route('user.index') }}" method="GET">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Name</label>
                                    <input class="form-control" type="text" name="name" value="{{ old('name', $request->name) }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Email</label>
                                    <input class="form-control" type="text" name="email" value="{{ old('email', $request->email) }}">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm ms-auto">Search</button>
                    </form>
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-header pb-0">
                    {{-- <h6>Users</h6> --}}
                    <div class="d-flex align-items-center">
                        <h6 class="mb-0">Users</h6>
                        <a href="{{ route('export.users.excel', $request->query()) }}" class="btn btn-success btn-sm ms-auto"><i class="fa fa-solid fa-file-excel"></i> Export Excel</a>
                        <a href="{{ route('export.users.pdf', $request->query()) }}" class="btn btn-info btn-sm mx-1"><i class="fa fa-solid fa-file-pdf"></i> Export PDF</a>

                        <a href="{{ route('user.create') }}" class="btn btn-primary btn-sm mx-1">Add New User</a>
                    </div>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0" id="userTable">
                            <thead>
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">ID</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Name</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Email</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Create Date</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>
                                            <p class="align-middle text-center text-sm">{{ $user->id }}</p>
                                        </td>
                                        <td>
                                            <div class="d-flex px-3 py-1">
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">{{ $user->name }}</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-sm">
                                            <p class="text-sm font-weight-bold mb-0">{{ $user->email }}</p>
                                        </td>
                                        <td class="align-middle text-center text-sm">
                                            <p class="text-sm font-weight-bold mb-0">{{ $user->created_at }}</p>
                                        </td>
                                        <td class="align-middle text-end">
                                            <div class="d-flex px-3 py-1 justify-content-center align-items-center">
                                                <a href="{{ route('user.edit', $user->id) }}"><button
                                                        class="btn btn-success btn-sm ms-auto">Edit</button></a>

                                                @if (Auth::user()->id != $user->id)
                                                    <form method="POST" action="{{ route('user.destroy', $user->id) }}">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}

                                                        <button class="btn btn-primary btn-sm ms-auto">Delete</button>
                                                    </form>
                                                @endif


                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-scripts')
    <script>
        $(document).ready(function() {
            $('#userTable').DataTable({
                "order": [
                    [0, "asc"],
                ],
            });
        });
    </script>
@endpush

<html>
    <head>
        <title>
            User List
        </title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="Content-Type">

        <style>
            body {
                padding: 0 25px 27px;
                font-family: Arial, sans-serif;
            }

            table,
            th,
            td {
                border-style: solid;
                border-width: thin;
                border-collapse: collapse;
            }

            /* table{
                table-layout: fixed;
                width: 100%
            } */
        </style>
    </head>

    <body>
        <div class="container mt-5">
            <h2 class="text-center">User List</h2>

            <table class="table table-bordered mb-5">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Create Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </body>
</html>

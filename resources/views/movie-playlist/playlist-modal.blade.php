<div class="modal fade" id="playlistModal" tabindex="-1" role="dialog" aria-labelledby="playlistModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="playlistModalLabel"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <img id="modal_poster_path" style="max-width: 50%" src="" alt="">
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="modal_description">Description</label>
                            <p class="text-primary text-small ms-1" id="modal_description"></p>
                        </div>
                        <div class="form-group">
                            <label for="modal_creator">Created By</label>
                            <p class="text-primary text-small ms-1" id="modal_creator"></p>
                        </div>
                        <div class="form-group">
                            <label for="modal_total_movies">Number of Movies</label>
                            <p class="text-primary text-small ms-1" id="modal_total_movies"></p>
                        </div>
                    </div>
                </div>
                <div class="row" id="movies_container">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@push('after-scripts')
    <script>
        $(document).ready(function() {
            const formatter = new Intl.NumberFormat('en-US', {
              style: 'currency',
              currency: 'USD',
            });

            $('body').on('click', '#playlistModalButton', function(event) {
                event.preventDefault();
                var id = $(this).data('id');
                $.get('/get-playlist/' + id,
                    function(data) {
                        console.log(data);
                        $('#playlistModalLabel').html(data.title);
                        $('#modal_description').html(data.description);
                        $('#modal_creator').html(data.creator_name);
                        $('#modal_total_movies').html(data.movies.length);

                        var html = '';
                        for (var i = 0; i < data.movies.length; i++) {
                            // console.log(data.movies);

                            html += `<div class="col-md-3 mt-4">
                                <div class="card">
                                  <img class="card-img-top" src="` + data.movies[i].poster_path + `">
                                  <div class="card-body">
                                    <h6>
                                        ` + data.movies[i].original_title + `
                                    </h6>
                                    <label class="ms-0">
                                        ` + moment(data.movies[i].release_date).format('D MMMM YYYY') + `
                                    </label>
                                  </div>
                                </div>
                              </div>`;


                        }
                        $('#movies_container').append($(html).hide().fadeIn(1000));

                    })

            });
        });
    </script>
@endpush

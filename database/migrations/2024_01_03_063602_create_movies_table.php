<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
  /**
   * Run the migrations.
   */
  public function up(): void
  {
    Schema::create('movies', function (Blueprint $table) {
      $table->id();
      $table->boolean('adult')->nullable();
      $table->string('backdrop_path')->nullable();
      $table->string('original_language')->nullable();
      $table->longText('original_title')->nullable();
      $table->longText('overview')->nullable();
      $table->decimal('popularity', 10, 2)->nullable();
      $table->string('poster_path')->nullable();
      $table->date('release_date')->nullable();
      $table->longText('title')->nullable();
      $table->boolean('video')->nullable();
      $table->decimal('budget', 15, 2)->nullable();
      $table->decimal('revenue', 15, 2)->nullable();
      $table->integer('runtime')->nullable();
      $table->longText('tagline')->nullable();
      $table->decimal('vote_average', 10, 2)->nullable();
      $table->integer('vote_count')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   */
  public function down(): void
  {
    Schema::dropIfExists('movies');
  }
};

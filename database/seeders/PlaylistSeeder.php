<?php

namespace Database\Seeders;

use App\Models\Playlist;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PlaylistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Playlist::create([
            'title' => 'Movies everyone should watch at least once during their lifetime',
            'description' => 'I asked reddit what’s one movie everyone should watch at least once in their lifetime to compile a list of movies that everyone should watch.',
            'creator_id' => User::all()->random(10)->first()->id,
        ]);

        Playlist::create([
            'title' => 'for when you want to feel something',
            'description' => 'this is just purely a list of movies that have impacted me and have made me reflect and change my view on some things.',
            'creator_id' => User::all()->random(10)->first()->id,
        ]);

        Playlist::create([
            'title' => 'You’re not the same person once the film has finished',
            'description' => 'grass is greener on the other side',
            'creator_id' => User::all()->random(10)->first()->id,
        ]);

        Playlist::create([
            'title' => 'A24 - Complete Filmography',
            'description' => 'Full list of A24\'s filmography, chronologically...',
            'creator_id' => User::all()->random(10)->first()->id,
        ]);

        Playlist::create([
            'title' => 'Good Films, 90 Minutes or Less',
            'description' => 'If you\'re feeling overwhelmed, but still want to squeeze a film into your daily routine, this list is made for you. Basically the only criteria is 90 minutes or less and I have to like it (or think that it looks interesting).',
            'creator_id' => User::all()->random(10)->first()->id,
        ]);
    }
}

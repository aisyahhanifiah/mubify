<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'name' => 'admin',
            'email' => 'admin@mubify.com',
            'password' => Hash::make('secret'),
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
        ]);

        User::factory()->count(100)->create();
    }
}

<?php

namespace Database\Seeders;

use App\Models\Movie;
use App\Models\Playlist;
use App\Models\PlaylistMovie;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PlaylistMovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for($x = 1; $x <= 70; $x++){
            PlaylistMovie::create([
                'movie_id' => Movie::all()->random(10)->first()->id,
                'playlist_id' => Playlist::all()->random(1)->first()->id,
            ]);
        }
    }
}

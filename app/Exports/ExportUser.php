<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ExportUser implements WithHeadings, FromQuery, WithMapping
{
    public $name;
    public $email;

    public function __construct($name, $email)
    {
        $this->name = $name;
        $this->email = $email;
    }

    public function query()
    {
        $users = User::query();

        if ($this->name) {
            $users->where('name', 'like', '%' . $this->name . '%');
        }

        if ($this->email) {
            $users->where('email', 'like', '%' . $this->email . '%');
        }

        return $users;
    }

    public function map($user): array
    {
        return [
            $user->id,
            $user->name,
            $user->email,
            $user->created_at,
        ];
    }

    public function headings(): array
    {
        return [
            'ID',
            'Name',
            'Email',
            'Create Date',
        ];
    }
}

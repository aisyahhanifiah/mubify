<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use App\Models\Playlist;
use Illuminate\Http\Request;

class MoviePlaylistController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $playlists = Playlist::query();
        $playlists = $this->filter($request, $playlists);
        $playlists = $playlists->orderBy('id', 'ASC')->paginate(8);

        $genres = Genre::orderBy('name')->get();

        return view('movie-playlist.index', compact('playlists', 'request', 'genres'));
    }

    public function getPlaylist($id)
    {
        $playlist = Playlist::with('movies')->with('creator')->find($id);
        $movies = $playlist->movies;

        $playlist['creator_name'] = $playlist->creator->name;

        return response()->json($playlist);
    }

    public function filter($request, $playlists)
    {
        if ($request->original_title) {
            $playlists = $playlists->whereRelation('movies', 'original_title', 'LIKE', '%' . $request->original_title . '%');
        }

        if ($request->genre) {
            $playlists = $playlists->whereRelation('movies.genres', 'genres.id', $request->genre);
        }

        return $playlists;
    }
}

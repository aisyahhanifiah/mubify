<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use App\Models\Movie;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $movies = Movie::query();
        $movies = $this->filter($request, $movies);
        $movies = $movies->orderBy('id', 'ASC')->paginate(8);

        $genres = Genre::orderBy('name')->get();
        return view('movie.index', compact('movies', 'request', 'genres'));
    }

    public function getMovie($id)
    {
        $movie = Movie::with('genres')->find($id);
        $movie['genre'] = implode(', ', $movie->genres->pluck('name')->toArray());
        return response()->json($movie);
    }

    public function loadMoreData(Request $request)
    {
        $start = $request->start;

        $movies = Movie::orderBy('id', 'ASC');
        $movies = $this->filter($request, $movies);
        $movies = $movies->offset($start)
            ->limit(8)
            ->get();

        return response()->json([
            'data' => $movies,
            'next' => $start + 8
        ]);
    }

    public function movie_list(Request $request)
    {
        $query = Movie::query()->select('movies.*')->limit(50);

        $query = $this->filter($request, $query);

        return DataTables::of($query)->addIndexColumn()
            ->addColumn('options', function (Movie $movie) {
                return  view('movie.options', ['movie' => $movie->id]);
            })->addColumn('genre', function (Movie $movie) {
                return implode(', ', $movie->genres->pluck('name')->toArray());
            })
            ->make(true);
    }

    public function filter($request, $movies)
    {
        if ($request->title) {
            $movies = $movies->where('original_title', 'LIKE', '%'.$request->title. '%');
        }
        if ($request->genre) {
            $movies = $movies->whereRelation('genres', 'genres.id', $request->genre);
        }

        return $movies;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use App\Models\Movie;
use App\Models\MovieGenre;
use Illuminate\Http\Request;

class SeedTMDBController extends Controller
{
    public function seedMovies()
    {
        $client = new \GuzzleHttp\Client();

        // goal get at least 20k movies
        for($y = 2000; $y < 2022; $y++){
            for($x = 1; $x < 20; $x++){
                $response = $client->request('GET', 'https://api.themoviedb.org/3/discover/movie?include_adult=false&include_video=false&language=en-US&page='.$x.'&sort_by=popularity.desc$year='. $y, [
                    'headers' => [
                        'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJhMDJiZDhkMGFkNWNhZDg0ZGYwODQ4YjIwNTJiZWUzNSIsInN1YiI6IjY1OTQzYzhkZTAwNGE2NmRiYjE3Zjc0YyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.8wBsAHDJq4A94LzxyvliuxiX5I8yS2e8GpjyKFd3nGE',
                        'accept' => 'application/json',
                    ],
                ]);

                $contents = json_decode($response->getBody(), true);

                $tmdb_movies = $contents['results'];

                foreach ($tmdb_movies as $tmdb_movie) {
                    $genre = Movie::find($tmdb_movie['id']);

                    if ($genre) {
                        $genre->update([
                            'adult' => $tmdb_movie['adult'],
                            'backdrop_path' => 'https://image.tmdb.org/t/p/w1280' . $tmdb_movie['backdrop_path'],
                            'original_language' => $tmdb_movie['original_language'],
                            'original_title' => $tmdb_movie['original_title'],
                            'overview' => $tmdb_movie['overview'],
                            'popularity' => $tmdb_movie['popularity'],
                            'poster_path' => 'https://image.tmdb.org/t/p/w500' . $tmdb_movie['poster_path'],
                            'release_date' => (isset($tmdb_movie['release_date']) && $tmdb_movie['release_date']) ? $tmdb_movie['release_date'] : NULL,
                            'title' => $tmdb_movie['title'],
                            'video' => $tmdb_movie['video'],
                            'vote_average' => $tmdb_movie['vote_average'],
                            'vote_count' => $tmdb_movie['vote_count'],
                        ]);
                    } else {
                        Movie::create([
                            'id' => $tmdb_movie['id'],
                            'adult' => $tmdb_movie['adult'],
                            'backdrop_path' => 'https://image.tmdb.org/t/p/w1280' . $tmdb_movie['backdrop_path'],
                            'original_language' => $tmdb_movie['original_language'],
                            'original_title' => $tmdb_movie['original_title'],
                            'overview' => $tmdb_movie['overview'],
                            'popularity' => $tmdb_movie['popularity'],
                            'poster_path' => 'https://image.tmdb.org/t/p/w500' . $tmdb_movie['poster_path'],
                            'release_date' => (isset($tmdb_movie['release_date']) && $tmdb_movie['release_date']) ? $tmdb_movie['release_date'] : NULL,
                            'title' => $tmdb_movie['title'],
                            'video' => $tmdb_movie['video'],
                            'vote_average' => $tmdb_movie['vote_average'],
                            'vote_count' => $tmdb_movie['vote_count'],
                        ]);
                    }
                }

                echo $x . '\n';
            }
        }

    }

    public function seedMovieGenres()
    {
        $client = new \GuzzleHttp\Client();

        $movie_ids = Movie::pluck('id');

        foreach($movie_ids as $movie_id)
        {
            $client = new \GuzzleHttp\Client();

            $response = $client->request('GET', 'https://api.themoviedb.org/3/movie/'.$movie_id.'?language=en-US', [
                'headers' => [
                    'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJhMDJiZDhkMGFkNWNhZDg0ZGYwODQ4YjIwNTJiZWUzNSIsInN1YiI6IjY1OTQzYzhkZTAwNGE2NmRiYjE3Zjc0YyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.8wBsAHDJq4A94LzxyvliuxiX5I8yS2e8GpjyKFd3nGE',
                    'accept' => 'application/json',
                ],
            ]);

            $contents = json_decode($response->getBody(), true);
            $tmdb_genres = $contents['genres'];

            Movie::find($movie_id)->update([
                'budget' => $contents['budget'],
                'revenue' => $contents['revenue'],
                'runtime' => $contents['runtime'],
                'tagline' => $contents['tagline'],
            ]);

            // foreach ($tmdb_genres as $tmdb_genre) {
            //     $movie_genre = MovieGenre::where('movie_id', $movie_id)->where('genre_id', $tmdb_genre['id'])->first();

            //     if ($movie_genre == NULL) {
            //         MovieGenre::create([
            //             'movie_id' => $movie_id,
            //             'genre_id' => $tmdb_genre['id']
            //         ]);
            //     }
            // }
        }
    }

    public function seedGenres()
    {
        $client = new \GuzzleHttp\Client();

        $response = $client->request('GET', 'https://api.themoviedb.org/3/genre/movie/list?language=en', [
          'headers' => [
            'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJhMDJiZDhkMGFkNWNhZDg0ZGYwODQ4YjIwNTJiZWUzNSIsInN1YiI6IjY1OTQzYzhkZTAwNGE2NmRiYjE3Zjc0YyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.8wBsAHDJq4A94LzxyvliuxiX5I8yS2e8GpjyKFd3nGE',
            'accept' => 'application/json',
          ],
        ]);

        $contents = json_decode($response->getBody(), true);
        $tmdb_genres = $contents['genres'];

        foreach($tmdb_genres as $tmdb_genre)
        {
            $genre = Genre::find($tmdb_genre['id']);

            if($genre){
                $genre->update([
                    'name' => $tmdb_genre['name']
                ]);
            }else{
                Genre::create([
                    'id' => $tmdb_genre['id'],
                    'name' => $tmdb_genre['name']
                ]);
            }
        }
    }
}

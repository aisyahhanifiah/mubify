<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Playlist;
use App\Models\User;
use App\Notifications\WelcomeUserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        // $users = User::all();
        // $users = User::orderBy('created_at', 'desc')->get();

        $users = User::query();

        if($request->name){
            $users->where('name', 'like', '%' . $request->name . '%');
        }

        if($request->email){
            $users->where('email', 'like', '%' . $request->email . '%');
        }

        $users = $users->orderBy('created_at', 'desc')->get();

        return view('user.index', compact('users', 'request'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255|email|unique:users,email',

        ]);


        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => 'secret'
        ]);

        Notification::send($user, new WelcomeUserNotification());

        return redirect()->route('user.index')->with(['status' => true, 'message' => 'Data has been successfully created.']);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $user = User::find($id);

        return view('user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $user = User::find($id);

        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $user = User::find($id);

        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255|email|unique:users,email,'.$user->id,
        ]);

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        return redirect()->route('user.index')->with(['status' => true, 'message' => 'Data has been succesfully updated.']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $user = User::find($id);

        Playlist::where('creator_id', $user->id)->forceDelete();

        $user->delete();

        return redirect()->route('user.index')->with(['status' => true, 'message' => 'Data has been succesfully deleted.']);
    }
}

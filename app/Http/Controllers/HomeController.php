<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $top_five_genres = DB::table('genres')
        ->select('genres.name', DB::raw('COUNT(movie_genres.genre_id) as total_movies'))
        ->leftJoin('movie_genres', 'genres.id', '=', 'movie_genres.genre_id')
        ->orderByRaw('COUNT(movie_genres.genre_id) DESC')
        ->groupBy('genres.name')
        ->limit(5)
        ->get();
        // SELECT a.name, COUNT(b.genre_id) AS TotalMovies
        // FROM genres AS a
        // LEFT JOIN movie_genres AS b ON a.id = b.genre_id
        // GROUP BY a.name
        // ORDER BY TotalMovies DESC
        // LIMIT 5;

        $top_five_playlists = DB::table('playlist_movies')
        ->select('playlist_movies.playlist_id', 'playlists.title', 'playlists.description', 'users.name', 'playlist_movies.movie_id', 'movies.backdrop_path', 'movies.popularity')
        ->leftJoin('playlists', 'playlists.id', '=', 'playlist_movies.playlist_id')
        ->leftJoin('movies', 'movies.id', '=', 'playlist_movies.movie_id')
        ->leftJoin('users', 'users.id', '=', 'playlists.creator_id')
        ->orderByRaw('ROW_NUMBER() OVER (PARTITION BY playlist_movies.playlist_id ORDER BY movies.popularity DESC)')
        ->limit(5)
        ->get();
        // SELECT playlist_movies.playlist_id, playlists.title, playlists.description, users.name, playlist_movies.movie_id, movies.backdrop_path, movies.popularity
        // FROM playlist_movies
        // LEFT JOIN movies ON movies.id = playlist_movies.movie_id
        // LEFT JOIN playlists ON playlists.id = playlist_movies.playlist_id
        // LEFT JOIN users ON users.id = playlists.creator_id
        // ORDER BY ROW_NUMBER() OVER (PARTITION BY playlist_movies.playlist_id ORDER BY movies.popularity DESC)
        // LIMIT 5
        // ;

        $top_five_profitable_genres = DB::table('movie_genres')
        ->selectRaw('genres.name, SUM(movies.budget) AS total_budget, SUM(movies.revenue) AS total_revenue, (SUM(revenue) - SUM(budget)) / SUM(budget) * 100 AS profit_percentage')
        ->leftJoin('genres', 'genres.id', '=', 'movie_genres.genre_id')
        ->leftJoin('movies', 'movies.id', '=', 'movie_genres.movie_id')
        ->groupBy('genres.name')
        ->orderByDesc('profit_percentage')
        ->limit(5)
        ->get();
        // SELECT genres.name, SUM(movies.budget) AS total_budget, SUM(movies.revenue) AS total_revenue,
        // (SUM(revenue) - SUM(budget)) / SUM(budget) * 100 AS profit_percentage
        // FROM movie_genres
        // LEFT JOIN genres ON genres.id = movie_genres.genre_id
        // LEFT JOIN movies ON movies.id = movie_genres.movie_id
        // GROUP BY genres.name
        // ORDER BY profit_percentage DESC
        // LIMIT 5;


        return view('pages.dashboard', compact('top_five_genres', 'top_five_playlists', 'top_five_profitable_genres'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SeedCountriesController extends Controller
{
    public function getLanguages()
    {
        $client = new \GuzzleHttp\Client();

        $response = $client->request('GET', 'https://restcountries.com/v3.1/all', [
            'headers' => [
                'accept' => 'application/json',
            ],
        ]);

        $contents = json_decode($response->getBody(), true);

        $languages = [];
        $pluck_languages = collect($contents)->pluck('languages')->toArray();

        foreach($pluck_languages as $pluck_language1){
            if(is_array($pluck_language1)){
                foreach($pluck_language1 as $key => $value){
                    if(!array_key_exists($key, $languages)){
                        $languages[$key] = $value;
                    }


                }
            }

        }

        $languages = array_unique($languages);
        sort($languages);
        return $languages;
    }
}

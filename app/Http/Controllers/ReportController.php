<?php

namespace App\Http\Controllers;

use App\Exports\ExportUser;
use App\Models\User;
use PDF;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    public function export_users_pdf(Request $request)
    {
        $users = User::query();

        if ($request->name) {
            $users->where('name', 'like', '%' . $request->name . '%');
        }

        if ($request->email) {
            $users->where('email', 'like', '%' . $request->email . '%');
        }

        $users = $users->get();

        $data = [
            'users' => $users
        ];

        $pdf = PDF::loadView('user.export-pdf', $data)->setPaper('a4', 'landscape');

        return $pdf->stream('user list.pdf');
    }

    public function export_users_excel(Request $request)
    {
        return Excel::download(new ExportUser($request->name, $request->email), 'users.xlsx');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;

class MovieAPIController extends Controller
{
    public function monthlyReleaseTrend($year)
    {
        $monthly_trend = collect();

        for($x = 1; $x <= 12; $x++){
            $total_movies = Movie::whereYear('release_date', $year)->whereMonth('release_date', $x)->get()->count();
            $monthly_trend->push($total_movies);
        }

        return $monthly_trend;
    }
}

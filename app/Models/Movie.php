<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Movie extends Model
{
    use HasFactory;

    protected $fillable = [
      'id',
      'adult',
      'backdrop_path',
      'original_language',
      'original_title',
      'overview',
      'popularity',
      'poster_path',
      'release_date',
      'title',
      'video',
      'budget',
      'revenue',
      'runtime',
      'tagline',
      'vote_average',
      'vote_count',
    ];

    protected $casts  = [
        'release_date' => 'date',
    ];

    public function genres(): BelongsToMany
    {
        return $this->belongsToMany(Genre::class, MovieGenre::class);
    }

    public function playlists(): BelongsToMany
    {
        return $this->belongsToMany(Playlist::class, PlaylistMovie::class);
    }
}

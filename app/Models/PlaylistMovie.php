<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlaylistMovie extends Model
{
    use HasFactory;

    protected $table = 'playlist_movies';

    protected $fillable = [
        'movie_id',
        'playlist_id',
    ];
}

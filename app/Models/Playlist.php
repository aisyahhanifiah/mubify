<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Playlist extends Model
{
  use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'creator_id',
    ];

    public function movies(): BelongsToMany
    {
        return $this->belongsToMany(Movie::class, PlaylistMovie::class);
    }

    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function getMostPopularPoster()
    {
        return $this->movies->sortByDesc('popularity')->first()->backdrop_path;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormatCurrency extends Model
{
    use HasFactory;

    public static function abbreviateNumber($num)
    {
        if ($num >= 0 && $num < 1000) {
            $format = floor($num);
            $suffix = '';
        } else if ($num >= 1000 && $num < 1000000) {
            $format = floor($num / 1000);
            $suffix = 'K+';
        } else if ($num >= 1000000 && $num < 1000000000) {
            $format = floor($num / 1000000);
            $suffix = 'M+';
        } else if ($num >= 1000000000 && $num < 1000000000000) {
            $format = floor($num / 1000000000);
            $suffix = 'B+';
        } else if ($num >= 1000000000000) {
            $format = floor($num / 1000000000000);
            $suffix = 'T+';
        }

        return !empty($format . $suffix) ? $format . $suffix : 0;
    }
}

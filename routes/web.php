<?php

use App\Http\Controllers\ReportController;
use App\Http\Controllers\SeedTMDBController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserProfileController;
use App\Http\Controllers\ResetPassword;
use App\Http\Controllers\ChangePassword;
use App\Http\Controllers\MovieAPIController;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\MoviePlaylistController;
use App\Http\Controllers\SeedCountriesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('get-languages', [SeedCountriesController::class, 'getLanguages']);
Route::get('seed-movies', [SeedTMDBController::class, 'seedMovies']);
Route::get('seed-genres', [SeedTMDBController::class, 'seedGenres']);
Route::get('seed-movie-genres', [SeedTMDBController::class, 'seedMovieGenres']);

Route::get('/export/users/pdf', [ReportController::class, 'export_users_pdf'])->name('export.users.pdf')->middleware('auth');
Route::get('/export/users/excel', [ReportController::class, 'export_users_excel'])->name('export.users.excel')->middleware('auth');
Route::resource('user', UserController::class)->middleware('auth');
Route::resource('movie', MovieController::class)->middleware('auth');
Route::resource('playlist', MoviePlaylistController::class)->middleware('auth');
Route::get('monthly-release-trend/{year}', [MovieAPIController::class, 'monthlyReleaseTrend'])->middleware('auth');
Route::get('movie-list', [MovieController::class, 'movie_list'])->middleware('auth');
Route::get('load-more-movies', [MovieController::class, 'loadMoreData'])->name('load.more.movies');
Route::get('load-more-playlists', [MoviePlaylistController::class, 'loadMoreData'])->name('load.more.playlists');
Route::get('get-movie/{movie_id}', [MovieController::class, 'getMovie']);
Route::get('get-playlist/{playlist_id}', [MoviePlaylistController::class, 'getPlaylist']);

Route::get('/', function () { return redirect('/dashboard'); })->middleware('auth');

Route::get('/register', [RegisterController::class, 'create'])->middleware('guest')->name('register');
Route::post('/register', [RegisterController::class, 'store'])->middleware('guest')->name('register.perform');
Route::get('/login', [LoginController::class, 'show'])->middleware('guest')->name('login');
Route::post('/login', [LoginController::class, 'login'])->middleware('guest')->name('login.perform');
Route::get('/reset-password', [ResetPassword::class, 'show'])->middleware('guest')->name('reset-password');
Route::post('/reset-password', [ResetPassword::class, 'send'])->middleware('guest')->name('reset.perform');
Route::get('/change-password', [ChangePassword::class, 'show'])->middleware('guest')->name('change-password');
Route::post('/change-password', [ChangePassword::class, 'update'])->middleware('guest')->name('change.perform');

Route::get('/dashboard', [HomeController::class, 'index'])->name('home')->middleware('auth');
Route::group(['middleware' => 'auth'], function () {
    Route::get('/virtual-reality', [PageController::class, 'vr'])->name('virtual-reality');
    Route::get('/rtl', [PageController::class, 'rtl'])->name('rtl');
    Route::get('/profile', [UserProfileController::class, 'show'])->name('profile');
    Route::post('/profile', [UserProfileController::class, 'update'])->name('profile.update');
    Route::get('/profile-static', [PageController::class, 'profile'])->name('profile-static');
    Route::get('/sign-in-static', [PageController::class, 'signin'])->name('sign-in-static');
    Route::get('/sign-up-static', [PageController::class, 'signup'])->name('sign-up-static');
    Route::get('/{page}', [PageController::class, 'index'])->name('page');
    Route::post('logout', [LoginController::class, 'logout'])->name('logout');
});
